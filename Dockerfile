FROM node:lts-alpine3.17 as builder

ENV NODE_ENV production

WORKDIR /app
COPY . .

RUN npm ci
RUN npm run build

FROM nginx:stable-alpine as production

COPY --from=builder /app/build /usr/share/nginx/html
COPY nginx.conf /etc/nginx/conf.d/default.conf

EXPOSE 3030

CMD ["nginx", "-g", "daemon off;"]