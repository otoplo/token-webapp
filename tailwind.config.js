/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./src/**/*.{js,jsx,ts,tsx,html,css}",
  ],
  theme: {
    screens: {
      '2xl': { 'max': '1400px' },

      'xl': { 'max': '1279px' },

      'lg': { 'max': '1023px' },

      'md': { 'max': '768px' },

      'sm': { 'min': '320px' },
    },
    extend: {
      gradientColorStops: {
        'custom-pink': '#F293FB',
        'custom-purple': '#8276F8',
        'custom-pink-1': '#2A14F6',
        'custom-purple-1': '#E409F9',
        customIndigo: 'rgba(42, 20, 246, 1)',
        customFuchsia: 'rgba(228, 9, 249, 1)',
        customGreen: 'rgba(19, 219, 135, 1)',
        customBlue: 'rgba(20, 178, 212, 1)',
        customWhite: 'rgba(239,62,160)',
        customRed: 'rgba(208,45,63)',
      },
      boxShadow: {
        'up': '5px 5px 20px 0px rgba(114, 114, 114, 1)',
        'down': '-5px -5px 20px 0px rgba(25, 25, 28, 1)',
      },
      fontSize: {
        clamp: "clamp(14px, 4vw, 15px)",
      },
    },
    fontFamily: {
      rajdhani: ["Rajdhani", "sans-serif"],
      sansArrow: ["PT Sans Narrow", "sans-serif"]
    },
  },
  plugins: [],
}
